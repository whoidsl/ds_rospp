import argparse
import os.path

def get_dir_partsctory(dirname):
    fullpath = os.path.realpath(dirname)
    if os.path.isdir(fullpath):
        return fullpath, ''
    else:
        return os.path.dirname(fullpath), os.path.basename(fullpath)

def splitEvery(w,n):
    for i in range(0, len(w),n):
        yield w[i:i+n]



def topic_to_varname(topic):
    """
    Convert a ROS topic name into a valid matlab variable name by replacing
    offensive characters

    :param topic: The raw ROS topic name
    :return: A sanitized, valid matlab variable naem
    """
    base = topic.replace('/', '_')

    # Strip leading underscores.  Usually at most one
    for i in range(0, len(base)):
        if str.isalpha(base[i]):
            return base[i:]

class ProgressBar(object):
    def __init__(self, width=80):
        self._total_width = width
        self._hash_width = self._total_width - 10

        self._current = 0

    def update(self, progress):
        """
        Update the progress bar based on some percentage
        :param progress: Progress completed, from 0 to 1
        :return:
        """
        to_print = int(progress * self._hash_width)
        if to_print != self._current:
            self._current = to_print
            #print '\r' * self._total_width,
            print '\r[ %s%s ] %4.0f%%' % ('#' * to_print, ' ' * (self._hash_width-to_print), progress*100),



