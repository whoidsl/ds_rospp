#!/bin/bash

# 2018/05/26
#
# Ian Vaughn (ivaughn@whoi.edu)
#
# This script reindexes any unindexed bag files and moves the resulting
# indexed files to the normal bag file extension.  This leaves only
# the *.orig.active files, which can be removed with an rm command.
#
# This is very helpful if ros kills your roslaunch session while still 
# indexing the bagfiles.
#
# This one does it all single-threaded.  For wimpy computers.

# Make sure we have a source directory
if [ ! -n "$1" ]
then
    echo "Usage: rosbag_reindex [rosbag directory]"
    exit $E_BADARGS
fi

# Get a list of all the active files in that directory.
ACTIVEFILES=("$1"/*.bag.active)

# Loop through once.  Reindex and move each file
for activefile in "${ACTIVEFILES[@]}"
do
    rootname="${activefile%%.*}"

    if [ -e "$rootname.bag" ]
    then
        echo "$rootname.bag already exists, skipping..."
        continue
    fi

    echo "Got filename $activefile -> $rootname"
    rosbag reindex "$activefile"
    echo "mv $activefile -> $rootname.bag"
    mv "$activefile" "$rootname.bag"
    
done

echo "It should now be safe to remove *.orig.active"
